import React, { useEffect, useState } from "react";
import { Container, CssBaseline, Grid, Typography } from "@material-ui/core";
import Header from "../../components/header-home";
import Dialog from "@material-ui/core/Dialog";
import Slide from "@material-ui/core/Slide";
import Clear from "@material-ui/icons/Clear";
import { withTransaction } from "@elastic/apm-rum-react";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function HomePage(props) {
  const { classes } = props;
  const [categories] = useState([]);
  const [allCategory, setAllCategory] = useState(false);
  const [categoryRow, setCategoryRow] = useState(null);
  const [enableFeaturedProduct, setEnableFeaturedProduct] = useState(null);


  useEffect(() => {
    setCategoryRow("1");
    setEnableFeaturedProduct(
      "true"
    );
  }, []);
  const fabStyle = () => {
      return { marginBottom: 56 }
  };
  return (
    <>
      <CssBaseline />
      <Container maxWidth="xs" className={classes.container} style={fabStyle()}>(
          <>
            <div
              style={{
                position: "fixed",
                maxWidth: 442,
                top: 0,

                zIndex: 99,
                height: 150,
                width: "100%",
                backgroundColor: "#fff",
                boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.1)"
              }}
            >
              <Header />
            </div>
            <br />
            <div className={classes.gridSlider} style={{ marginTop: 143 }}>
            </div>
            {categoryRow === "2" ? (
              <div className={classes.gridCategories}>
                {categories.length >= 11 ? (
                  <>
                    <div className={classes.gridCategory}>
                      {categories.slice(0, 5).map(item => {
                        return (
                          <div style={{ width: "20%" }}>
                              image={item.image.url}
                              name={item.name}
                              id={item.id}
                          </div>
                        );
                      })}
                      <div
                        style={{ width: "20%" }}
                        onClick={() => {
                          setAllCategory(true);
                        }}
                      >
                      </div>
                    </div>
                  </>
                ) : (
                  <>
                    <div className={classes.gridCategory}>
                      {categories.slice(0, 5).map(item => {
                        return (
                          <div style={{ width: "20%" }}>
                          </div>
                        );
                      })}
                    </div>
                    <div
                      className={classes.gridCategory}
                      style={{ marginTop: 12 }}
                    >
                      {categories.slice(5, 10).map(item => {
                        return (
                          <div style={{ width: "20%" }}>
                          </div>
                        );
                      })}
                    </div>
                  </>
                )}
              </div>
            ) : (
              <div className={classes.gridCategories}>
                {categories.length >= 6 ? (
                  <>
                    <div className={classes.gridCategory}>
                      {categories.slice(0, 4).map(item => {
                        return (
                          <div style={{ width: "20%" }}>
                          </div>
                        );
                      })}
                      <div
                        style={{ width: "20%" }}
                        onClick={() => {
                          setAllCategory(true);
                        }}
                      >
                      </div>
                    </div>
                  </>
                ) : (
                  <>
                    <div className={classes.gridCategory}>
                      {categories.slice(0, 5).map(item => {
                        return (
                          <div style={{ width: "20%" }}>
                          </div>
                        );
                      })}
                    </div>
                  </>
                )}
              </div>
            )}

            {enableFeaturedProduct === "true" ? (
              <div className={classes.gridTopSeller}>
                <Grid
                  item
                  xs={12}
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    padding: "16px 16px 20px"
                  }}
                >
                  <Grid item xs={9}>
                    <Typography style={{ fontSize: 13, fontWeight: 700 }}>
                      Produk Pilihan
                    </Typography>
                    <Typography
                      style={{
                        fontSize: 10,
                        fontWeight: 400,
                        color: "#252525"
                      }}
                    >
                      Produk pilihan yang terbaik untukmu
                    </Typography>
                  </Grid>
                  <Grid
                    item
                    xs={3}
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "flex-end"
                    }}
                  >
                    <span
                      className={classes.moreText}
                      onClick={() => {
                        props.history.push("/top-seller");
                      }}
                    >
                      Lihat Semua
                    </span>
                  </Grid>
                </Grid>
                <Grid className={classes.scrollingWrapper}>
                  <Grid className={classes.sliderMarginEnd} />
                </Grid>
              </div>
            ) : (
              <></>
            )}
          </>
        )
        <div
          style={{
            display: "flex",
            justifyContent: "flex-end"
          }}
        >
        </div>

        <Dialog
          classes={{
            paperFullScreen: classes.fullPaper,
            scrollPaper: classes.paper
          }}
          fullScreen
          open={allCategory}
          onClose={event => {
            setAllCategory(false);
          }}
          TransitionComponent={Transition}
        >
          <div className={classes.header}>
            <div
              style={{
                display: "flex",
                width: "100%",
                justifyContent: "center"
              }}
            >
              <div
                style={{
                  backgroundColor: "#F1F2F6",
                  width: "8%",
                  height: 4,
                  borderRadius: 10
                }}
              ></div>
            </div>
            <div
              style={{
                display: "flex",
                width: "100%",
                justifyContent: "flex-end",
                paddingRight: 15
              }}
            >
              <Clear
                style={{ color: "grey" }}
                onClick={() => setAllCategory(false)}
              />
            </div>
          </div>
          <div style={{ marginTop: 85, borderTop: "2px solid #fafafa" }}>
            <div className={classes.gridCategories}>
              <div className={classes.gridCategory}>
                {categories.map(item => {
                  return (
                    <div style={{ width: "20%" }}>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </Dialog>
      </Container>
    </>
  );
}

export default withTransaction("HomePage", "component")(HomePage);