import React from "react";
import { withRouter } from "react-router-dom";
import { Grid, Typography, InputBase } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";

function Component(props) {
  const { classes } = props;

  return (
    <>
      <Grid container className={classes.container} item xs={12}>
        <div className={classes.locationDiv}>
          <Grid item xs={9} className={classes.locationGrid}>
            <div>
              <Typography className={classes.titleText}>
                Kamu berbelanja di
              </Typography>
            </div>
            <div
              style={{ display: "flex", flexDirection: "row", marginTop: 6 }}
            >
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
              </div>
              <Typography
                style={{ fontSize: "11px", fontWeight: 700, marginLeft: 8 }}
              >
              </Typography>
            </div>
          </Grid>
          <Grid item xs={3} className={classes.changeGrid}>
          </Grid>
        </div>
        <div
          className={classes.searchDiv}
          onClick={() => {
            props.history.push("/product-search");
          }}
        >
          <div className={classes.searchIcon}>
            <SearchIcon />
          </div>
          <InputBase
            placeholder="Cari produk yang kamu cari ..."
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            InputProps={{
              "aria-label": "search",
            }}
          />
        </div>
      </Grid>
    </>
  );
}

export default withRouter(Component);
