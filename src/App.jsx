import React from 'react'
import './App.css';
import {Route, BrowserRouter as Router} from "react-router-dom"
import BottomNavigationApp from "./components/bottom-navigation";
import Home from "./containers/home"

import Container from '@material-ui/core/Container'
function App() {
  return (
    <>
    <Router>
      <Container maxWidth="xs">
        <Route exact path="/" component={Home}/>
      </Container>
        <>
          <BottomNavigationApp />
        </>
    </Router>
    </>
  );
}

export default App;
